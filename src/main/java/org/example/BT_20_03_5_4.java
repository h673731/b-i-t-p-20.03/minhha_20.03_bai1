package org.example;

import java.util.Scanner;

public class BT_20_03_5_4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Nhap so nguyen duong n = ");
        int n = scanner.nextInt();

        int tong = 0;
        for (int i = 0; i <= n; i++) {
            if (i % 2 == 0) {
                tong = tong + i;
            }
        }
        System.out.println("Tong cac so chan = " + tong);
    }
}

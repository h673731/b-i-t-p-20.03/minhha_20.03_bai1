package org.example;

import java.util.Scanner;

public class BT_20_03_5_1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Nhap so t1: ");
        int start = scanner.nextInt();
        System.out.print("Nhap so t2: ");
        int end = scanner.nextInt();

        System.out.println("Cac so nguyen to tu " + start + " den " + end + " la:");
        for (int i = start; i <= end; i++) {
            if (soNguyeTo(i)) {
                System.out.println(i);
            }
        }
    }

    private static boolean soNguyeTo(int number) {
        if (number < 2) {
            return false;
        }
        for (int i = 2; i < number; i++) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }
}

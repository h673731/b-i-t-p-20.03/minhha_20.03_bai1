package org.example;

import java.util.Scanner;

public class BT_20_03_4_1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Nhap ten hoc sinh 1: ");
        String tenHocSinh1 = scanner.nextLine();
        System.out.println("Nhap diem hoc ky hoc sinh 1: ");
        double diemHocSinh1 = scanner.nextDouble();

        scanner.nextLine();

        System.out.println("Nhap ten hoc sinh 2: ");
        String tenHocSinh2 = scanner.nextLine();
        System.out.println("Nhap diem hoc ky hoc sinh 2: ");
        double diemHocSinh2 = scanner.nextDouble();

        if (diemHocSinh1 > diemHocSinh2) {
            System.out.println(tenHocSinh1);
        } else {
            System.out.println(tenHocSinh2);
        }
    }
}

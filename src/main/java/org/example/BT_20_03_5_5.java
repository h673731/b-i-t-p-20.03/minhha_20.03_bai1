package org.example;

public class BT_20_03_5_5 {
    public static void main(String[] args) {
        StringBuilder result = new StringBuilder();
        for (int i = 10; i <= 200; i++) {
            if (i % 7 == 0 && i % 5 != 0) {
                result.append(i).append(",");
            }
        }
        System.out.println(result);
    }
}

package org.example;

import java.util.Scanner;

public class BT_20_03_5_3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Nhập số lương: ");
        double luong = scanner.nextDouble();

        double thue;
//        if (luong <= 5000000) {
//            thue = luong * 0.05;
//        } else if (luong <= 10000000) {
//            thue = luong * 0.1;
//        } else if (luong <= 18000000) {
//            thue = luong * 0.15;
//        } else if (luong <= 32000000) {
//            thue = luong * 0.2;
//        } else if (luong <= 52000000) {
//            thue = luong * 0.25;
//        } else if (luong <= 80000000) {
//            thue = luong * 0.3;
//        } else {
//            thue = luong * 0.35;
//        }
        switch ((int) luong) {
            case 0:
                thue = luong * 0.05;
                break;
            case 5000000:
                thue = luong * 0.1;
                break;
            case 10000000:
                thue = luong * 0.15;
                break;
            case 18000000:
                thue = luong * 0.2;
                break;
            case 32000000:
                thue = luong * 0.25;
                break;
            case 52000000:
                thue = luong * 0.3;
                break;
            case 80000000:
                thue = luong * 0.35;
                break;
            default:
                thue = luong * 0.35;
                break;
        }
        String formattedThue = String.format("%.0f", thue);
        System.out.println("So thue phai nop: " + formattedThue);
    }
}

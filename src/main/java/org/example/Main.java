package org.example;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Nhập vào số N: ");
        int soN = scanner.nextInt();
        int tong = tinhTong(soN);
        scanner.close();
        System.out.println(tong);
    }

    public static int tinhTong(int soN) {
        int tong = 0;
        if (soN >= 0) {
            for (int i = 0; i <= soN; i += 2) {
                tong += i;
            }
            return tong;
        }
        return 10000;
    }

}
package org.example;

public class BT_20_03_4_4 {
    public static void main(String[] args) {
        int intNumber1 = 10;
        double doubleNumber1 = intNumber1;
        System.out.println("Ep kieu ngam dinh: " + doubleNumber1);

        double doubleNumber2 = 10.5;
        int intNumber2 = (int) doubleNumber2;
        System.out.println("Ep kieu tuong minh: " + intNumber2);

        Integer integerObject1 = 5;
        System.out.println("Autoboxing: " + integerObject1);

        Integer integerObject2 = 10;
        int intNumber3 = integerObject2;
        System.out.println("Unboxing: " + intNumber3);
    }
}

package org.example;

import java.util.Scanner;

public class BT_20_03_5_2 {
    public static void main(String[] args) {
        int[] danhsach = new int[10];

        Scanner scanner = new Scanner(System.in);
        for (int i = 0; i < danhsach.length; i++) {
            System.out.print("So thu " + (i + 1) + ": ");
            danhsach[i] = scanner.nextInt();
        }

        for(int i = 0; i < danhsach.length - 1; i++) {
            for(int j = 0; j < danhsach.length - 1; j++) {
                if(danhsach[j] > danhsach[j+1]){
                    int temp = danhsach[j];
                    danhsach[j] = danhsach[j+1];
                    danhsach[j+1] = temp;
                }
            }
        }
        System.out.println("Mang sau sap xep: ");
        for (int i : danhsach) {
            System.out.print(i + " ");
        }
    }
}
